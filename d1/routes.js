const http = require("http");

// storing the 4000 in a variable called port
const port = 4000;


// storing the createServer method inside the server variable
const server = http.createServer((request, response) => {
    /* 
        "request" is an object that is sent via the client (browser)
        "url" is the property of the request that refers to the endpoint of the link
    */
    // the condition below means need to access the url with "/greeting"
    if (request.url === "/greeting") 
    {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Hi there");
    } 
    else if (request.url === "/homepage") 
    {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Hello");
    }
    /* 
        All other routes will return a message "404: Page not found" if the url is not existing
    */ 
    else 
    {
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.end("Page not found");
    }
});

server.listen(port);


console.log(`Server now running at port:${port}`);
