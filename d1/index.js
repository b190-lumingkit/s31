/* 
    require() - used to load Node.js modules

    module - a software component/part of a program that contains one or more routines

    "http" - lets Node.js transfer data using HyperText Transfer Protocol; set of individual files
        contain code to create a "component" that helps establish data transfer between applications

    HTTP - protocol that allows fetching of resources as HTML documents

    Clients (browsers) and server (Node.js/Express.js applications) communicate by exchanging individual messages

    Message that came from the clients - request
    Mesages that came from the server - response
*/
let http = require("http");

/* 
    http - we are now trying to use the http module to create our server-side application

    createServer() - found inside http module; a method that accepts a function as its argument for a creation of a server

    (request, response) - arguments that are passed to the createServer method; this would allow us to receive requests (1st parameter) and send responses (2nd parameter)
*/
http.createServer(function (request, response) {
    /* 
        writeHead()
            - set a status code for the response - 200 means OK/successful status
            - set Content-Type; using "text/plain" means sending plain text as a response
        
    */
    response.writeHead(200, {"Content-Type" : "text/plain"});
    /* 
        response.end()
            - denote the last stage of the communication which is the sending of the response
    */
    // this code below will send the response with text content "Hello World"
    response.end("Hello World");


// .listen() allows the application to run in local devices through a specified port.
/* 
    port - virtual point where network connections start and end
    each port is associated with a specific process/service
*/
// the code below means that the server will be assigned to port 4000 via .listen(4000) method where the server will listen to any request that are sent to it eventually communicating with our server
}).listen(4000);

/* 
    use node index.js to run the server
    press ctrl + C to terminate gitbash process
*/

// to confirm if the server is running on a port
console.log("Server running at port 4000");
console.log("Hello World");

/* 
    nodemon
        - npm package, allows server to automatically restart when files have been changed for update i.e saving files
*/

